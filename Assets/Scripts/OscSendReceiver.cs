/* ***************************************************************************
 * Roberto Fazio Studio 2016 
 * Last Update: 28.06.2016
 * 
 * ***************************************************************************
 */
using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading;

public class OscSendReceiver : MonoBehaviour 
{
	public string	        	RemoteIP;
	public static int 			SendToPort; // Sendo To Arduino UNO Ethernet 
	public static int 			ListenerPort = 12000;
	public static Osc 			handler;
	public static UDPPacketIO 	udp;
	public  string 				address;
	public  string    			values;

	void Awake()
	{
		udp = gameObject.AddComponent<UDPPacketIO>() as UDPPacketIO;
		handler = gameObject.AddComponent<Osc>() as Osc;
	}

	void Start () 
	{	
		RemoteIP = XmlConfig.ip.InnerXml.ToString();
		SendToPort = int.Parse(XmlConfig.port.InnerXml.ToString());

		udp.init(RemoteIP, SendToPort, ListenerPort);
		handler.init(udp);

		// handler for a specific event
		handler.SetAddressHandler("/ard/aaa", ListenEvent);

		// set a handler for all incoming messages
		handler.SetAllMessageHandler(ListenAllEvent);

		print("remote ip: " + RemoteIP + " port: " + SendToPort);

	}

	public void ListenEvent(OscMessage oscMessage)
	{	
		address = oscMessage.Address;

		if(address == "/ard/aaa")
		{
			print(oscMessage.Address + " : " + oscMessage.Values[0]);
		}

	} 
<<<<<<< HEAD

	// Send 100 pixels encoded to Arduino
	public static void PixelsPackageOSC(string EncodedPixels)
	{
		if (LoadEncode.data != null) 
		{
			string msg = "/unity/values " + EncodedPixels;
			OscMessage oscM = Osc.StringToOscMessage(msg);
			handler.Send(oscM);
			//print("osc msg  sended: " + msg);

		}
	}



public static void ResetServo()
{
	OscMessage oscM = Osc.StringToOscMessage ("/unity/reset ");
	handler.Send(oscM);
	print ("OSC sent: ResetServo");
}


void Update()
{
	if (LoadEncode.data != null) 
	{
		// Test sender check frame
		if (Input.GetKeyDown (KeyCode.A)) 
		{
			string prima = "1111111111111111111111111111111111111111111111111100000000000000000000";
			string seconda = "000000000000000000000000000000";
			OscSendReceiver.PixelsPackageOSC (prima);
			OscSendReceiver.PixelsPackageOSC ("a" + seconda);
			print (prima.Length + " " + seconda.Length);
=======

	// Send 100 pixels encoded to Arduino
	public static void PixelsPackageOSC(string EncodedPixels)
	{
		if (LoadEncode.data != null) 
		{
			//EncodedPixels = LoadEncode.data;
			//string m = "/unity/values " + EncodedPixels; // dopo m ci va uno spazio vuoto

			string msg = "/unity/values " + EncodedPixels;
<<<<<<< HEAD

			OscMessage oscM = Osc.StringToOscMessage(msg);

			handler.Send(oscM);
			print("osc msg  sended: " + msg);
=======

			OscMessage oscM = Osc.StringToOscMessage(msg);

			handler.Send(oscM);
			print("osc msg  sended: " + msg);

>>>>>>> f966fdfd854e43f407c42a3c1a29f018493f4a4f
		}
	}


<<<<<<< HEAD
		if (Input.GetKeyDown (KeyCode.B)) 
		{
			string prima = "0000000000000000000000000000000000000000000000000011111111111111111111";
		    string seconda = "111111111111111111111111111111";
			OscSendReceiver.PixelsPackageOSC (prima);
			OscSendReceiver.PixelsPackageOSC ("a" + seconda);
			print (prima.Length + " " + seconda.Length);

		}

	}

	if (Input.GetKeyDown (KeyCode.R))
		ResetServo ();
}

=======

	void Update()
	{
		if (LoadEncode.data != null) 
		{
			if (Input.GetKeyDown (KeyCode.Space)) 
			{	
				string firstPart = LoadEncode.data.Substring (0, 70);

				print ("firstPart: " + firstPart + "lenght: " + firstPart.Length);

				OscSendReceiver.PixelsPackageOSC(firstPart);

			}
				

			if (Input.GetKeyDown (KeyCode.T)) 
			{
				string secondPart = LoadEncode.data.Substring (69, 30);

				print ("secondPart: " + secondPart + "lenght: " + secondPart.Length);
				//HACK antepondo un carattere non numerico al secondo pacchetto. Mistero!
				OscSendReceiver.PixelsPackageOSC("a" + secondPart);
			}
>>>>>>> ffee47bdd3f7627f3b823d77ea1c9ae00c605d13

		}
	}

<<<<<<< HEAD
	public static void ResetServo()
	{
		OscMessage oscM = Osc.StringToOscMessage ("/unity/reset ");
		handler.Send(oscM);
		print ("OSC sent: ResetServo");
	}


	void Update()
	{
		if (LoadEncode.data != null) 
		{
			if (Input.GetKeyDown (KeyCode.Space)) 
			{	
				string firstPart = LoadEncode.data.Substring (0, 70);
				print ("firstPart: " + firstPart + "lenght: " + firstPart.Length);
				OscSendReceiver.PixelsPackageOSC(firstPart);
			}


			if (Input.GetKeyDown (KeyCode.T)) 
			{
				string secondPart = LoadEncode.data.Substring (70, 30);
				print ("secondPart: " + secondPart + "lenght: " + secondPart.Length);
				//HACK antepondo un carattere non numerico al secondo pacchetto. Mistero!
				OscSendReceiver.PixelsPackageOSC("a" + secondPart);
			}

			if (Input.GetKeyDown (KeyCode.A)) 
			{
				string prima = "1111111111111111111111111111111111111111111111111100000000000000000000";
				string seconda = "000000000000000000000000000000";
				OscSendReceiver.PixelsPackageOSC (prima);
				OscSendReceiver.PixelsPackageOSC ("a" + seconda);
				print (prima.Length + " " + seconda.Length);
			}

			if (Input.GetKeyDown (KeyCode.B)) 
			{
				string prima = "0000000000000000000000000000000000000000000000000011111111111111111111";
			    string seconda = "111111111111111111111111111111";
				OscSendReceiver.PixelsPackageOSC (prima);
				OscSendReceiver.PixelsPackageOSC ("a" + seconda);
				print (prima.Length + " " + seconda.Length);

			}
>>>>>>> f966fdfd854e43f407c42a3c1a29f018493f4a4f

		}

<<<<<<< HEAD
public void ListenAllEvent(OscMessage oscMessage)
{
	address = oscMessage.Address.ToString();
	values = oscMessage.Values[0].ToString();
=======
		if (Input.GetKeyDown (KeyCode.R))
			ResetServo ();
	}

=======
>>>>>>> ffee47bdd3f7627f3b823d77ea1c9ae00c605d13
>>>>>>> f966fdfd854e43f407c42a3c1a29f018493f4a4f

	try { print(oscMessage.Address + " : " + oscMessage.Values[0]); } 
	catch (System.Exception ex) { print(ex.Message); }

<<<<<<< HEAD
}
=======
		try { print(oscMessage.Address + " : " + oscMessage.Values[0]); } 
		catch (System.Exception ex) { print(ex.Message); }

	}
>>>>>>> f966fdfd854e43f407c42a3c1a29f018493f4a4f





}